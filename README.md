# Matemáticas

# ______________________________________________________________________
# Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  node{
  lineColor purple
  }
  leafNode {
    LineColor white
    RoundCorner 0
    Shadowing 0.0
  }
  arrow {
    LineColor purple
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .purple {
    BackgroundColor lightPurple
  }
  
  .cyan {
    BackgroundColor lightCyan
  }
}
</style>
* Conjuntos, Aplicaciones y funciones<<cyan>>

**_ en las
*** Operaciones <<rose>>
****_ con
***** Insercción
******_ nos permite 
******* formar un conjunto
********_ sólo con los 
********* elementos
**********_ comunes 
*********** involucrados 
************_ en la 
************* operación
******_ El 
******* símbolo 
********_ que se usa para 
********* indicar 
*********_ la 
********** operación de intersección 
***********_ es 
************ ∩
***** Complemento
******_ es el 
******* conjunto 
*******_ No pertenece a un
******** conjunto dado
******_ nos permite 
******* formar un conjunto 
********_ con todos los 
********* elementos 
**********_ del conjunto de 
*********** referencia 
*********** universal
************_ que no están en el 
************* conjunto
***** Unión
******_ nos 
******* Permite unir 
********_ dos o más 
********* conjuntos 
**********_ para 
*********** formar 
************_ otro conjunto que contendrá 
************* todos los elementos 
**************_ que queremos 
*************** unir 
**************_ pero sin que se 
*************** repitan
******_ El 
******* Símbolo 
********_ Que indica la 
********* Operación de unión
**********_ es 
*********** ∪

**_ se 
*** Representa <<rose>>
****_ con el 
***** Diagrama de Venn
******_ usa
******* círculos 
********_ para 
********* ilustrar 
**********_ las 
*********** relaciones lógicas 
************_ entre dos o más 
************* conjuntos de elementos
****_ también 
***** denominados 
******_ como
******* diagramas de conjunto
******* diagramas lógicos
****_ se 
***** usan ampliamente 
******_ en las áreas de 
******* matemática
******* estadística
******* lógica
******* enseñanza
******* lingüística
******* informática 
******* negocios
****_ lleva
***** el nombre del 
******_ lógico británico
******* John Venn.

**_ tipos de
*** Conjuntos << rose>>
**** finito
*****_ Es aquel 
****** conjunto 
*******_ con 
******** cardinalidad definida
**** infinito
*****_ Es aquél cuya 
****** cardinalidad 
*******_ no está 
******** definida
*********_ por ser 
********** demasiado grande 
***********_ para 
************ cuantificarlo
**** Vacío
*****_ Es aquel que 
****** carece 
*******_ de 
******** elementos


** Razonamientos <<rose>>
*** Abstracto 
****_ cerca de 
***** la filosofia

** Soluciones <<rose>>

*** Relacion 
**** Definición
****_  de pertenencia
***** Conjunto
****** Cardinal de un conjunto
******* numero natural
********_ según 
********* los elementos 
**********_ que 
*********** contenga
******* Formulas
******** Acotacion entre cardinales de conjuntos 
*********_ Es más 
********** Compleja
******** Cardinal de la union de conjuntos 
******_ Se
******* Representan 
********_ mediante 
********* El diagrama de venn
******_ Tipos
******* Universal
******* El vacio
****** relacion de orden
******* los elementos del primero 
******** Aparecen en el segundo 
***** Elemento
******_ No 
******* Pertenece 
********_ a un 
********* Conjunto dado

** Aplicaciones y funcionnes <<rose>>
*** Aplicacion 
****_ contiene
***** tipos 
******_ como son
******* inyectiva
******* subyectiva
******* biyectiva
****_ es una
*** Transformación
****_ de un 
***** agente externo
******_ En el cual
******* convierte
*******_ a cada uno de los
******** Elementos
*********_ de un 
********** Conjunto origen 
***********_ en un unico
************ conjunto final
****** identifica
******* que elemento del primero se convierte en el segundo 
**** Composiciones 
*****_ son
****** transformacion de transformacion 
****** composicion de aplicaciones 
****_ convierte un 
***** elemento del conjunto 
******_ a a un unico elemento 
****_ debe 
***** tener 
******_ un 
******* transformado
*** Función 
****_ los
***** conjuntos 
******_ que se 
******* transforman 
********_ son 
********* conjuntos 
**********_ de numeros naturales
*********** fraccionarios 
*********** reales
*********** terminologia clasica 
*********** aplicaciones 
************_ faciles de leer por 
************* los numeros 
****_ la palabra \nse utiliza para
***** los Conjuntos
****_ Consiste en la
***** transformación
******_ entre los 
******* conjuntos de numeros
****_ Son
***** Aplicaciones
******_ mas faciles de
******* Ver
*******_ si se grafican
******** los ejes Coordenados
*********_ En el eje de las
********** Ordenadas
********** Abscisas


*** subconjunto imagen e imagen inversa


@endmindmap
```

# __________________________________________________________________
# Funciones (2010)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  node{
  lineColor purple
  }
  leafNode {
    LineColor white
    RoundCorner 0
    Shadowing 0.0
  }
  arrow {
    LineColor purple
  }
  .lila {
    BackgroundColor line
  }
  .rose {
    BackgroundColor #FFBBCC
  
}
</style>
* Funciones

**_ Sus 
*** Limites <<lila>>
****_ son 
***** Continuas 
******_  su 
******* gráfica 
********_ puede 
********* dibujarse 
**********_ de 
*********** un solo trazo
***** Discontinuas
******_ No hay
******* limite
*******_ en la
******** función
******_ presenta algún 
******* punto 
********_ en el que existe 
********* un salto 
**********_ y la gráfica 
*********** se rompe.

**_ es
*** el corazon de las matemáticas <<lila>>
****_ es una 
***** aplicación
******_ que es
******* especial
****_  es un 
***** conjunto 
******_ que está
******* relacionado 
********_ con
********* conjuntos de numeros 
**********_ son
*********** Numeros realies


**_ sus
*** caracteristicas <<lila>>
****_ Para cada 
***** valor x 
******_ del 
******* dominio 
********_ le corresponde un
********* único valor 
****_ Una 
***** función 
******_ es una 
******* relación 
********_ entre dos 
********* conjuntos
****_ Las 
***** funciones 
*****_ describen 
****** fenómenos
*******_ de la vida 
******** cotidiana
****_ Se pueden 
***** representar 
******_ de manera
******* gráfica

****_ tienen
***** continuidad
******_ No produce
******* saltos
******* discontinuidad
******_ son 
******* funciones
********_ más 
********* manejables
***** decrecientes
***** Maximos y minimos
******_ en 
******* Maximos
********_ Empieza a
********* Disminuir <<lila>>
**********_ el 
*********** valor
******* Minimos
********_ Empieza a 
********* Aumentar <<lila>>

***** dominio <<rose>>
******_ Es el 
******* conjunto 
********_ de todos los
********* valores 
**********_ de la 
*********** variable independiente

***** Rango o imagen 
******_ Es el 
******* conjunto 
********_ de todos los
********* valores <<rose>>
**********_ de la 
*********** variable dependiente

***** Codominio
******_ es el 
******* conjunto de valores
********_  que podrían 
********* salir <<rose>>
**********_  Este 
*********** conjunto 
************_ contiene al 
************* conjunto imagen 
**************_ de la 
*************** función.

**_ la
*** idea 
****_ surge del
***** cambio 
*****_ dónde las
****** Matemáticas
******_ buscan como
******* aprovechar 
********_ los 
********* cambios

*** Representacion cartesiana 
****_ se
***** representa 
******_ en un 
******* Plano
********_ Por medio de
********* ejes
********** Eje de las x
*********** horizontal
********** Eje de las y
*********** vertical
****_ depende del
***** comportamiento 
******_ de la 
******* funcion
********_ surgen 
********* Maximos 
**********_ cuando deja de 
*********** Aumentar <<rose>>
********* Minimos
**********_ cuando deja de 
*********** Disminuir <<rose>>

** Refleja 
***_ el 
**** Pensamiento 
*****_ del 
****** hombre
***_ la 
**** comprensión
*****_ de su 
****** entorno
*******_ con ayuda de las
******** Matematicas <<rose>>
*********_ resolver 
********** Problemas cotidianos <<lila>>

**_ su
*** Aplicación
****_ son en
***** Transformaciones
******_ de los 
******* Conjuntos 
********_ en otros
********* conjuntos
***** Conjuntos
******_ entre
******* conjuntos
********_ que estan 
********* relacionados <<lila>>



@endmindmap
```
# __________________________________________________________________
# La matemática del computador (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  node{
  lineColor purple
  }
  arrow {
    LineColor purple
  }
  .purple {
    BackgroundColor lightPurple
  }
</style>
* Matemática del computador <<purple>>

**_ tiene 
*** Manejo 
***_ de numeros
**** Grandes y Pequeños
*****_ Según 
****** El producto
******* Numero
********_ cuando se
********* multiplica 
**********_ por una 
*********** Potencia
******* Notacion
********_ entre
********* Exponencial Normalizada
********* Cientifica
******* Magnitud
********_ del 
********* Numero

** Utiliza
***_ la 
**** Aritmetica finita
*****_ tiene
****** limitaciones
*******_ en los 
******** Medios fisicos
*********_ tiene
********** Limitacion de decimales
****** Numeros finitos 
*******_ de 
******** posiciones
********* convierte a series de posiciones

*** Sistema Binario
****_ este es 
***** Simple
******_ basado en
******* Numeros 
********_ de 
********* 0
********* 1
***** Básico
******_ es el
******* Sistema de la computadora

***_ Su  
**** codificación
*****_ Representa una
****** Serie de caracteristicas
*******_ Entre ellas
******** Numeros 
*********_ Se pueden llevar a cabo
********** Operaciones
********* complementos
********* Conversores
*********_ como
********** Paso de corriente
********** cargas
******** Letras

** Precisión finita
***_ surge a travez de 
**** calculos
*****_ con 
****** Menor dificultad
****** Maquina mecanica
*******_ con 
******** Posiciones
*********_ de 
********** representaciones 
***********_ de 
************ digitos
************ Cifras
*****_ hacen 
****** referencia
*******_ a la
******** Aritmetica infinita
*****_ Contienen
****** Teorias
****** Abstraccion

**_ tiene
*** Operaciónes binarias
****_ como
***** Sumas
***** Restas


**_ la
*** Investigación asistida 
****_ por 
***** computadora 
****_ en 
***** diversas áreas 
******_ de las 
******* matemáticas
********_ como 
********* la lógica 
**********_ con la 
*********** demostración automática 
************_ de 
************* teoremas
********* Las matemáticas discretas 
**********_ con la 
*********** búsqueda 
************_ de 
************* estructuras matemáticas
**************_  tales como 
*************** grupos
********* la teoría de números 
**********_ Aplicando
*********** pruebas de primalidad 
*********** factorización

**_ se 
*** Aplica 
****_ en 
***** Geometría
******_ de tipo
******* algebraica computacional
****** computacional
***** Teoría 
******_ de 
******* grupos computacional
******* números computacional
******** algorítmica 
********_ de 
********* la información
********* juegos
***** Topología computacional
******_ es una
******* red 
********_ para 
********* intercambiar datos
***** Estadística computacional

@endmindmap
```

# ____________________________________________________________________

